﻿using DoctorDatabase.Helpers;
using DoctorDatabase.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Phone.UI.Input;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace DoctorDatabase
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ViewModelLocator _vmlocator = new ViewModelLocator();

        bool isNavigationBack = false;
        
        public MainPage()
        {
            this.InitializeComponent();
           
            this.NavigationCacheMode = NavigationCacheMode.Required;
#if WINDOWS_PHONE_APP
            Messenger.Default.Register<Type>(this, Constants.NavigationToken, (TargetPageType) => this.Frame.Navigate(TargetPageType));
#endif

            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
           
        }

        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            

            if (_vmlocator.Main.SelectedPivotIndex > 0 )
            {
                _vmlocator.Main.SelectedPivotIndex = 0;
                e.Handled = true;
                isNavigationBack = false;
            }


        }

        

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            //if (e.NavigationMode == NavigationMode.Back)
            //    isNavigationBack = true;
            //else
            //    isNavigationBack = false;
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
            base.OnNavigatedFrom(e);
        }

        
    }
}
