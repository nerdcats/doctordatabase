﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DoctorDatabase.Model
{
    public class RatingData
    {
        public string _id { get; set; }
        public double Rating { get; set; }
        public string Comment { get; set; }
        public int DoctorId { get; set; }
        public string TimeStamp { get; set; }
        public string Name { get; set; }
    }

    public class RatingRequest
    {
        public string Name { get; set; }
       // public string _id { get; set; }
        public double Rating { get; set; }
        public string Comment { get; set; }
        public int DoctorId { get; set; }
        public string DeviceId { get; set; }
    }

    public class RatingRequestFeedBack
    {
        public string Status { get; set; }
    }
}
