﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace DoctorDatabase.Model
{
    public class Doctor: ObservableObject
    {
        public string Address { get; set; }
        public string Area { get; set; }
        public string ChamberTime { get; set; }
        public string Expertise { get; set; }
        public string Name { get; set; }
        public List<object> Phone { get; set; }
        public double Rating { get; set; }
        public List<object> WeekDays { get; set; }
        public int _id { get; set; }

        public string PicUrl { get; set; }
        public bool isFavourite { get; set; }

    }

    public class QueryResponse
    {
        public List<Doctor> data { get; set; }
        public int count { get; set; }
        public int startIncolledex { get; set; }
    }
}
