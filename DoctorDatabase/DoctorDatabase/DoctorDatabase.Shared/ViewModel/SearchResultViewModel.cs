﻿using DoctorDatabase.Helpers;
using DoctorDatabase.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.Text;

namespace DoctorDatabase.ViewModel
{
    public class SearchResultViewModel : ViewModelBase
    {
        private QueryHelper _qHelper = new QueryHelper();

        public const string SearchResultsPropertyName = "SearchResults";

        private ObservableCollection<Doctor> _searchResults = null;
        public ObservableCollection<Doctor> SearchResults
        {
            get
            {
                return _searchResults;
            }

            set
            {
               

                _searchResults = value;
                RaisePropertyChanged(SearchResultsPropertyName);
            }
        }

      
        public const string QueryObjectPropertyName = "QueryObject";

        private dynamic _QueryObject = null;
        public dynamic QueryObject
        {
            get
            {
                return _QueryObject;
            }

            set
            {
                if (_QueryObject == value)
                {
                    return;
                }

               

               
                _QueryObject = value;
                HandleQuery(_QueryObject);
                RaisePropertyChanged(QueryObjectPropertyName);
            }
        }

        public const string DownloadFailedPropertyName = "DownloadFailed";

        private bool _downloadFailed = false;
        public bool DownloadFailed
        {
            get
            {
                return _downloadFailed;
            }

            set
            {
                if (_downloadFailed == value)
                {
                    return;
                }


                _downloadFailed = value;
                RaisePropertyChanged(DownloadFailedPropertyName);
            }
        }


        public const string CommErrorPropertyName = "CommError";
        private bool _commError = false;
        public bool CommError
        {
            get
            {
                return _commError;
            }

            set
            {
                if (_commError == value)
                {
                    return;
                }


                _commError = value;
                RaisePropertyChanged(CommErrorPropertyName);
            }
        }

        

        public const string ProgressRingVisibilityPropertyName = "ProgressRingVisibility";

        private bool _ProgressRingVisbility = false;

        public bool ProgressRingVisibility
        {
            get
            {
                return _ProgressRingVisbility;
            }

            set
            {
                if (_ProgressRingVisbility == value)
                {
                    return;
                }
    
                _ProgressRingVisbility = value;
                RaisePropertyChanged(ProgressRingVisibilityPropertyName);
            }
        }

        
    
        public const string EmptyResultPropertyName = "EmptyResult";

        private bool _EmptyResult = false;
        public bool EmptyResult
        {
            get
            {
                return _EmptyResult;
            }

            set
            {
                if (_EmptyResult == value)
                {
                    return;
                }

                
                _EmptyResult = value;
                RaisePropertyChanged(EmptyResultPropertyName);
            }
        }


        public const string ShowDoctorDetailsCommandPropertyName = "ShowDoctorDetailsCommand";

        private RelayCommand<Doctor> _ShowDoctorDetailsCommand = null;
        public RelayCommand<Doctor> ShowDoctorDetailsCommand
        {
            get
            {
                return _ShowDoctorDetailsCommand;
            }

            set
            {
                if (_ShowDoctorDetailsCommand == value)
                {
                    return;
                }
                _ShowDoctorDetailsCommand = value;
                RaisePropertyChanged(ShowDoctorDetailsCommandPropertyName);
            }
        }

        private ViewModelLocator _vmlocator = new ViewModelLocator();


        public const string RefreshSearchCommandPropertyName = "RefreshSearchCommand";
        private RelayCommand _refreshSearchCommand = null;
        public RelayCommand RefreshSearchCommand
        {
            get
            {
                return _refreshSearchCommand;
            }

            set
            {
                if (_refreshSearchCommand == value)
                {
                    return;
                }

                _refreshSearchCommand = value;
                RaisePropertyChanged(RefreshSearchCommandPropertyName);
            }
        }
        

        public SearchResultViewModel()
        {
            ShowDoctorDetailsCommand = new RelayCommand<Doctor>(ShowDoctorsDetailsAction);
            RefreshSearchCommand = new RelayCommand(RefreshSearchAction);
        }

        private void RefreshSearchAction()
        {
            if (QueryObject != null)
                HandleQuery(QueryObject);
        }

        private void ShowDoctorsDetailsAction(Doctor obj)
        {

            try
            {
                _vmlocator.DoctorDetails.DoctorDetailsDataContext = obj;
                Messenger.Default.Send<Type>(typeof(DoctorDetails), Constants.NavigationToken);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Navigation Failure");
                
            }
           
        }

        private bool propertyExists(dynamic Dynamique, string Property)
        {
            IDictionary<string, object> properties = (IDictionary<string, object>)Dynamique;
            return properties.ContainsKey(Property);
        }

        private void DeleteProperty( dynamic obj, string prop)
        {
            
            ((IDictionary<string, object>)obj).Remove(prop);
        }

        private async void HandleQuery(dynamic query)
        {
            ProgressRingVisibility = true;

#if WINDOWS_PHONE_APP
            ProgressIndicatorHelper ProgressHelper = new ProgressIndicatorHelper();
            ProgressHelper.ShowProgressIndicator("Searching....");
#endif
            try
            {
                int skip=0;
                DoctorDatabase.Helpers.QueryHelper.DoctorSortParams Sortparam = QueryHelper.DoctorSortParams.Name;

                SearchResults = null;

                if(propertyExists(query, "Sort"))
                {
                    Sortparam = (QueryHelper.DoctorSortParams)query.Sort;
                    DeleteProperty(query, "Sort");
                }

                if(propertyExists(query, "Skip"))
                {
                    skip = (int)query.Skip;
                    DeleteProperty(query, "Skip");
                }

                var result = await _qHelper.SearchDoctor(query, skip, Sortparam);
                SearchResults = new ObservableCollection<Doctor>(result);

                EmptyResult = SearchResults.Count > 0 ? false : true;

                DownloadFailed = false;
                CommError = false;

            }
            catch (Exception ex)
            {
                if (ex is HttpRequestException)
                    DownloadFailed = true;
                else
                    CommError = true;

            }
#if WINDOWS_PHONE_APP
            ProgressHelper.HideProgressIndicator();
#endif
            ProgressRingVisibility = false;
        }


        
    }
}
