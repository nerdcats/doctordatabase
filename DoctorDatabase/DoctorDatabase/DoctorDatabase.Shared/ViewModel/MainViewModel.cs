﻿using DoctorDatabase.Helpers;
using DoctorDatabase.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;

namespace DoctorDatabase.ViewModel
{
    public class MainViewModel: ViewModelBase
    {
        private ViewModelLocator _vmLocator = new ViewModelLocator();
       
        public const string IsAppBarEnabledPropertyName = "IsAppBarEnabled";

        private bool _isAppBarEnabled = true;
        public bool IsAppBarEnabled
        {
            get
            {
                return _isAppBarEnabled;
            }

            set
            {
                if (_isAppBarEnabled == value)
                {
                    return;
                }

               
                _isAppBarEnabled = value;
                RaisePropertyChanged(IsAppBarEnabledPropertyName);
            }
        }
        
        
      
        public const string NameTextBoxTextPropertyName = "NameTextBoxText";

        private string _nameTextBoxText = string.Empty;
        public string NameTextBoxText
        {
            get
            {
                return _nameTextBoxText;
            }

            set
            {
                if (_nameTextBoxText == value)
                {
                    return;
                }

                
                _nameTextBoxText = value;
                RaisePropertyChanged(NameTextBoxTextPropertyName);
            }
        }
        
        public const string AreaCollectionPropertyName = "AreaCollection";

        private ObservableCollection<string> _AreaCollection = null;
        public ObservableCollection<string> AreaCollection
        {
            get
            {
                return _AreaCollection;
            }

            set
            {
                if (_AreaCollection == value)
                {
                    return;
                }

                _AreaCollection = value;
                RaisePropertyChanged(AreaCollectionPropertyName);
            }
        
        }

        
        public const string SelectedPivotIndexPropertyName = "SelectedPivotIndex";

        private int _selectedPivotIndex = 0;

      
        public int SelectedPivotIndex
        {
            get
            {
                return _selectedPivotIndex;
            }

            set
            {
                if (_selectedPivotIndex == value)
                {
                    return;
                }

                
                _selectedPivotIndex = value;

                HandlePivotSelectionChanged(_selectedPivotIndex);

                RaisePropertyChanged(SelectedPivotIndexPropertyName);
            }
        }

        private void HandlePivotSelectionChanged(int _selectedPivotIndex)
        {
            if (_selectedPivotIndex == 1) //The search page, take off the appbar
                IsAppBarEnabled = false;
            else
                IsAppBarEnabled = true;

        }

        
        public const string SelectedAreaIndexPropertyName = "SelectedAreaIndex";

        private int _selectedAreaIndex = -1 ;

        
        public int SelectedAreaIndex
        {
            get
            {
                return _selectedAreaIndex;
            }

            set
            {
                if (_selectedAreaIndex == value)
                {
                    return;
                }

                
                _selectedAreaIndex = value;
                RaisePropertyChanged(SelectedAreaIndexPropertyName);
            }
        }


        public const string ExpertiseCollectionPropertyName = "ExpertiseCollection";
        private ObservableCollection<string> _expertiseCollection = null;
        public ObservableCollection<string> ExpertiseCollection
        {
            get
            {
                return _expertiseCollection;
            }

            set
            {
                if (_expertiseCollection == value)
                {
                    return;
                }

                
                _expertiseCollection = value;
                RaisePropertyChanged(ExpertiseCollectionPropertyName);
            }
        }


        public const string SelectedExpertiseIndexPropertyName = "SelectedExpertiseIndex";
        private int _selectedExpertiseIndex = -1;
        public int SelectedExpertiseIndex
        {
            get
            {
                return _selectedExpertiseIndex;
            }

            set
            {
                if (_selectedExpertiseIndex == value)
                {
                    return;
                }

                _selectedExpertiseIndex = value;
                RaisePropertyChanged(SelectedExpertiseIndexPropertyName);
            }
        }


        public const string SelectedSortOptionPropertyName = "SelectedSortOption";

        private int _SelectedSortOption = -1;
        public int SelectedSortOption
        {
            get
            {
                return _SelectedSortOption;
            }

            set
            {
                if (_SelectedSortOption == value)
                {
                    return;
                }

                
                _SelectedSortOption = value;
                RaisePropertyChanged(SelectedSortOptionPropertyName);
            }
        }

        private QueryHelper _qHelper = new QueryHelper();



      
        public const string DownloadFailedPropertyName = "DownloadFailed";

        private bool _downloadFailed = false;
        public bool DownloadFailed
        {
            get
            {
                return _downloadFailed;
            }

            set
            {
                if (_downloadFailed == value)
                {
                    return;
                }

           
                _downloadFailed = value;
                RaisePropertyChanged(DownloadFailedPropertyName);
            }
        }


        public const string CommErrorPropertyName = "CommError";
        private bool _commError = false;
        public bool CommError
        {
            get
            {
                return _commError;
            }

            set
            {
                if (_commError == value)
                {
                    return;
                }

                
                _commError = value;
                RaisePropertyChanged(CommErrorPropertyName);
            }
        }
        public const string SearchCommandPropertyName = "SearchCommand";

        private RelayCommand _searchCommand = null;

        public RelayCommand SearchCommand
        {
            get
            {
                return _searchCommand;
            }

            set
            {
                if (_searchCommand == value)
                {
                    return;
                }

                _searchCommand = value;
                RaisePropertyChanged(SearchCommandPropertyName);
            }
        }


        public const string GoToFavouritesCommandPropertyName = "GoToFavouritesCommand";

        private RelayCommand _GoToFavouritesCommand = null;
        public RelayCommand GoToFavouritesCommand
        {
            get
            {
                return _GoToFavouritesCommand;
            }

            set
            {
                if (_GoToFavouritesCommand == value)
                {
                    return;
                }

          
                _GoToFavouritesCommand = value;
                RaisePropertyChanged(GoToFavouritesCommandPropertyName);
            }
        }


        public const string GoToTopDoctorCategoriesCommandPropertyName = "GoToTopDoctorCategoriesCommand";
        private RelayCommand _GoToTopDoctorsCommand = null;
        public RelayCommand GoToTopDoctorCategoriesCommand
        {
            get
            {
                return _GoToTopDoctorsCommand;
            }

            set
            {
                if (_GoToTopDoctorsCommand == value)
                {
                    return;
                }

               
                _GoToTopDoctorsCommand = value;
                RaisePropertyChanged(GoToTopDoctorCategoriesCommandPropertyName);
            }
        }


        public const string SortOptionsCollectionPropertyName = "SortOptionsCollection";
        private ObservableCollection<string> _SortOptionsCollection = null;
        public ObservableCollection<string> SortOptionsCollection
        {
            get
            {
                return _SortOptionsCollection;
            }

            set
            {
                if (_SortOptionsCollection == value)
                {
                    return;
                }

                _SortOptionsCollection = value;
                RaisePropertyChanged(SortOptionsCollectionPropertyName);
            }
        }

    
        public const string ClearSelectionPropertyName = "ClearSelection";
        private RelayCommand<string> _ClearSelection = null;
        public RelayCommand<string> ClearSelection
        {
            get
            {
                return _ClearSelection;
            }

            set
            {
                if (_ClearSelection == value)
                {
                    return;
                }

               
                _ClearSelection = value;
                RaisePropertyChanged(ClearSelectionPropertyName);
            }
        }

  
        public const string RateAppCommandPropertyName = "RateAppCommand";

        private RelayCommand _RateAppCommand = null;
        public RelayCommand RateAppCommand
        {
            get
            {
                return _RateAppCommand;
            }

            set
            {
                if (_RateAppCommand == value)
                {
                    return;
                }

               
                _RateAppCommand = value;
                RaisePropertyChanged(RateAppCommandPropertyName);
            }
        }


        public const string ReinitializeCommandPropertyName = "ReinitializeCommand";
        private RelayCommand _ReinitializaCommand = null;
        public RelayCommand ReinitializeCommand
        {
            get
            {
                return _ReinitializaCommand;
            }

            set
            {
                if (_ReinitializaCommand == value)
                {
                    return;
                }

                
                _ReinitializaCommand = value;
                RaisePropertyChanged(ReinitializeCommandPropertyName);
            }
        }

        public const string IsInitializingPropertyName = "IsInitializing";
        private bool _isInitializing = false;
        public bool IsInitializing
        {
            get
            {
                return _isInitializing;
            }

            set
            {
                if (_isInitializing == value)
                {
                    return;
                }
                _isInitializing = value;
                RaisePropertyChanged(IsInitializingPropertyName);
            }
        }

       
        public const string GoToSearchPageCommandPropertyName = "GoToSearchPageCommand";

        private RelayCommand _GoToSearchPageCommand = null;

        
        public RelayCommand GoToSearchPageCommand
        {
            get
            {
                return _GoToSearchPageCommand;
            }

            set
            {
                if (_GoToSearchPageCommand == value)
                {
                    return;
                }

                
                _GoToSearchPageCommand = value;
                RaisePropertyChanged(GoToSearchPageCommandPropertyName);
            }
        }

        public MainViewModel()
        {

            Initialize();
            PopulateSortOptions();

            SearchCommand = new RelayCommand(SearchAction);
            GoToFavouritesCommand = new RelayCommand(GoToFavouritesAction);
            GoToTopDoctorCategoriesCommand = new RelayCommand(GoToTopDoctorCategories);
            ClearSelection = new RelayCommand<string>(ClearSelectionAction);
            RateAppCommand = new RelayCommand(RateAppAction);
            ReinitializeCommand = new RelayCommand(ReinitializeAction);
            GoToSearchPageCommand = new RelayCommand(GoToSearchPageAction);
            
        }

        private void GoToSearchPageAction()
        {
            SelectedPivotIndex = 1;
        }

        private void ReinitializeAction()
        {
            Initialize();
            PopulateSortOptions();
        }

        private async void RateAppAction()
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=" + CurrentApp.AppId));
        }

        private void ClearSelectionAction(string param)
        {
            if(param=="Sort")
            {
                SelectedSortOption = -1;
            }
        }

        private void PopulateSortOptions()
        {
            SortOptionsCollection = new ObservableCollection<string>();
            SortOptionsCollection.Add("Name");
            SortOptionsCollection.Add("Rating");
        }



        private void GoToTopDoctorCategories()
        {
            Messenger.Default.Send<Type>(typeof(TopDoctorCategories), Constants.NavigationToken);
        }

        private void GoToFavouritesAction()
        {
            Messenger.Default.Send<Type>(typeof(FavouritesPage), Constants.NavigationToken);
        }

        private void SearchAction()
        {

            try
            {
                DownloadFailed = false;
                CommError = false;
            

                string[] sep = { " " };

                dynamic query = new ExpandoObject();

                if (!string.IsNullOrWhiteSpace(NameTextBoxText))
                {
                    //var splits = NameTextBoxText.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    //query.Name = string.Concat(".*", string.Join(@".*", splits), ".*");
                    query.Name = NameTextBoxText;
                }
                if (SelectedAreaIndex >0)
                {
                    query.Area = AreaCollection[SelectedAreaIndex];
                }
                if (SelectedExpertiseIndex >0)
                {
                    query.Expertise = ExpertiseCollection[SelectedExpertiseIndex];
                }
                if(SelectedSortOption>0)
                {
                    query.Sort = QueryHelper.DoctorSortParams.Rating;
                }

                

                _vmLocator.Search.QueryObject = query;
                Messenger.Default.Send<Type>(typeof(SearchResults), Constants.NavigationToken);

                

                DownloadFailed = false;
                CommError = false;
            
            }
            catch (Exception ex)
            {
                if (ex is HttpRequestException)
                    DownloadFailed = true;
                else
                    CommError = true;
                
            }

        }

        private async void Initialize()
        {
#if WINDOWS_PHONE_APP
            ProgressIndicatorHelper progressHelper = new ProgressIndicatorHelper();
            progressHelper.ShowProgressIndicator("Initializing Doctor Areas and Expertise...");
#endif
            IsInitializing = true;
            try
            {
                await LoadAreas();
                await LoadExpertise();
                DownloadFailed = false;
                CommError = false;
            }
            catch (HttpRequestException excep)
            {
                DownloadFailed = true;
                
            }
            catch (Exception ex)
            {
                CommError = true;
            }
#if WINDOWS_PHONE_APP
            progressHelper.HideProgressIndicator();
#endif
            IsInitializing = false;
            
        }

        private async Task LoadAreas()
        {
            var areas = await _qHelper.GetAreasList();
            areas.Insert(0, "All");
            AreaCollection = new ObservableCollection<string>(areas);

           
        }

        private async Task LoadExpertise()
        {
            
            var expertises = await _qHelper.GetExpertiseList();
            expertises.Insert(0, "All");
            ExpertiseCollection = new ObservableCollection<string>(expertises);
        }


    }
}
