﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Text;
using DoctorDatabase.ViewModel;

namespace DoctorDatabase.ViewModel
{
    public class ViewModelLocator
    {
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public SearchResultViewModel Search
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SearchResultViewModel>();
            }
        }

        public DoctorDetailsViewModel DoctorDetails 
        { 
            get
            {
                return ServiceLocator.Current.GetInstance<DoctorDetailsViewModel>();
            }
            
        }

        public FavouritesViewModel Favourites
        {
            get
            {
                return ServiceLocator.Current.GetInstance<FavouritesViewModel>();
            }

        }

        public TopDoctorViewModel TopDoctor
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TopDoctorViewModel>();
            }

        }

        

        

        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<SearchResultViewModel>();
            SimpleIoc.Default.Register<DoctorDetailsViewModel>();
            SimpleIoc.Default.Register<FavouritesViewModel>();
            SimpleIoc.Default.Register<TopDoctorViewModel>();
            
        }
    }
}