﻿using DoctorDatabase.Helpers;
using DoctorDatabase.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Net.Http;
using System.Text;
using Windows.Storage.Streams;
using Windows.System.Profile;
using Windows.UI.Popups;
using System.Linq;

namespace DoctorDatabase.ViewModel
{
    public class DoctorDetailsViewModel: ViewModelBase
    {
 
        public const string DoctorDetailsDataContextPropertyName = "DoctorDetailsDataContext";

        private Doctor _DoctorDetailsDataContext = null;
        public Doctor DoctorDetailsDataContext
        {
            get
            {
                return _DoctorDetailsDataContext;
            }
            set
            {
                _DoctorDetailsDataContext = value;
                RaisePropertyChanged(DoctorDetailsDataContextPropertyName);
               
            }
        } 

 

        
        public const string NoReviewsPropertyName = "NoReviews";

        private bool _noReviews = false;

        public bool NoReviews
        {
            get
            {
                return _noReviews;
            }

            set
            {
                if (_noReviews == value)
                {
                    return;
                }

               
                _noReviews = value;
                RaisePropertyChanged(NoReviewsPropertyName);
            }
        }



  
        public const string ReviewDownloadFailedPropertyName = "ReviewDownloadFailed";

        private bool _reviewDownloadFailed = false;
        public bool ReviewDownloadFailed
        {
            get
            {
                return _reviewDownloadFailed;
            }

            set
            {
                if (_reviewDownloadFailed == value)
                {
                    return;
                }
                _reviewDownloadFailed = value;

               
                RaisePropertyChanged(ReviewDownloadFailedPropertyName);
            }
        }

 


        public const string isFlyoutOpenPropertyName = "isFlyoutOpen";

        private bool _isFlyoutOpen = false;

        public bool isFlyoutOpen
        {
            get
            {
                return _isFlyoutOpen;
            }

            set
            {
                if (_isFlyoutOpen == value)
                {
                    return;
                }

               
                _isFlyoutOpen = value;
                if (isFlyoutOpen)
                    isAppBarVisible = false;
                else
                    isAppBarVisible = true;

                RaisePropertyChanged(isFlyoutOpenPropertyName);
            }
        }



        public const string isAppBarVisiblePropertyName = "isAppBarVisible";

        private bool _isAppBarVisible = true;
        public bool isAppBarVisible
        {
            get
            {
                return _isAppBarVisible;
            }

            set
            {
                if (_isAppBarVisible == value)
                {
                    return;
                }

               
                _isAppBarVisible = value;
                RaisePropertyChanged(isAppBarVisiblePropertyName);
            }
        }


        public const string CommentTextPropertyName = "CommentText";

        private string _CommentText = string.Empty;
        public string CommentText
        {
            get
            {
                return _CommentText;
            }

            set
            {
                if (_CommentText == value)
                {
                    return;
                }

               
                _CommentText = value;
                RaisePropertyChanged(CommentTextPropertyName);
            }
        }


        public const string NameTextPropertyName = "NameText";

        private string _nameText = string.Empty;
        public string NameText
        {
            get
            {
                return _nameText;
            }

            set
            {
                if (_nameText == value)
                {
                    return;
                }

                
                _nameText = value;
                RaisePropertyChanged(NameTextPropertyName);
            }
        }

       
        public const string GivenRatePropertyName = "GivenRate";

        private double _givenRate = 1.00;
        public double GivenRate
        {
            get
            {
                return _givenRate;
            }

            set
            {
                if (_givenRate == value)
                {
                    return;
                }

               
                _givenRate = value;
                RaisePropertyChanged(GivenRatePropertyName);
            }
        }

    
    
       
        public const string ReviewsCollectionPropertyName = "ReviewsCollection";

        private ObservableCollection<RatingData> _ReviewsCollection = null;

        public ObservableCollection<RatingData> ReviewsCollection
        {
            get
            {
                return _ReviewsCollection;
            }

            set
            {
                if (_ReviewsCollection == value)
                {
                    return;
                }
                _ReviewsCollection = value;
                RaisePropertyChanged(ReviewsCollectionPropertyName);
            }
        }

        
        public const string CallNumberCommandPropertyName = "CallNumberCommand";

        private RelayCommand<string> _CallNumberCommand = null;

       
        public RelayCommand<string> CallNumberCommand
        {
            get
            {
                return _CallNumberCommand;
            }

            set
            {
                if (_CallNumberCommand == value)
                {
                    return;
                }

                
                _CallNumberCommand = value;
                RaisePropertyChanged(CallNumberCommandPropertyName);
            }
        }



        public const string RateDoctorCommandPropertyName = "RateDoctorCommand";

        private RelayCommand _RateDoctorCommand = null;
        public RelayCommand RateDoctorCommand
        {
            get
            {
                return _RateDoctorCommand;
            }

            set
            {
                if (_RateDoctorCommand == value)
                {
                    return;
                }

                _RateDoctorCommand = value;
                RaisePropertyChanged(RateDoctorCommandPropertyName);
            }
        }



        public const string AddToFavouritesPropertyName = "AddToFavourites";
        private RelayCommand _AddToFavorites = null;
        public RelayCommand AddToFavourites
        {
            get
            {
                return _AddToFavorites;
            }

            set
            {
                if (_AddToFavorites == value)
                {
                    return;
                }

                
                _AddToFavorites = value;
                RaisePropertyChanged(AddToFavouritesPropertyName);
            }
        }

        public DoctorDetailsViewModel()
        {
            CallNumberCommand = new RelayCommand<string>(CallNumberAction);
            RateDoctorCommand = new RelayCommand(RateDoctor);
            AddToFavourites = new RelayCommand(AddToFavouritesAction);
        }

        private void AddToFavouritesAction()
        {
            var FavouritesList= StorageHelper.LoadFavourites();
            if(!FavouritesList.Any(x=>x._id==_DoctorDetailsDataContext._id))
            {
                FavouritesList.Add(_DoctorDetailsDataContext);
                StorageHelper.SaveFavourites( FavouritesList);

                MessageDialog dialog = new MessageDialog(_DoctorDetailsDataContext.Name + " is added on your favourites");
                dialog.ShowAsync();
            }
            else
            {
                MessageDialog dialog = new MessageDialog(_DoctorDetailsDataContext.Name + " is already on your favourites");
                dialog.ShowAsync(); 
            }
        }

        

        private void CallNumberAction(string obj)
        {
#if WINDOWS_PHONE_APP
            Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI(obj, _DoctorDetailsDataContext.Name);
#endif
        }

        public async void GetDoctorRatings()
        {
#if WINDOWS_PHONE_APP
            ProgressIndicatorHelper ProgressHelper = new ProgressIndicatorHelper();
            ProgressHelper.ShowProgressIndicator("Fetching Reviews....");
#endif
            try
            {
                if (_DoctorDetailsDataContext != null)
                {
                    QueryHelper _qhelper = new QueryHelper();
                    var result = await _qhelper.GetRatingFeed(_DoctorDetailsDataContext._id);
                    ReviewsCollection = new ObservableCollection<RatingData>(result);
                    if (ReviewsCollection.Count > 0)
                        NoReviews = false;
                    else
                        NoReviews = true;
                    
                    ReviewDownloadFailed = false;
                }
            }
            catch (Exception ex)
            {
                if (ex is HttpRequestException)
                    ReviewDownloadFailed = true;
                else
                    NoReviews = true;

            }

#if WINDOWS_PHONE_APP
            ProgressHelper.HideProgressIndicator();
#endif
        }

        private async void RateDoctor()
        {
            if(string.IsNullOrEmpty(NameText) || string.IsNullOrEmpty(CommentText.Trim()))
            {
                MessageDialog mDialog = new MessageDialog("Please put your name and comment in the proper field", "Rate A Doctor");
                await mDialog.ShowAsync();
                isFlyoutOpen = true;
            }
            else
            {
	                bool result = false;
                    if (_DoctorDetailsDataContext!=null)
                    {
#if WINDOWS_PHONE_APP
                        ProgressIndicatorHelper ProgressHelper = new ProgressIndicatorHelper();
                       
#endif
                        try
                        {
                            QueryHelper _qHelper = new QueryHelper();
                            RatingRequest req = new RatingRequest();
                            req.DeviceId = GetHardwareId();
                            req.Name = NameText;
                            req.Comment = CommentText;
                    
                            req.DoctorId = _DoctorDetailsDataContext._id;
                            req.Rating = Math.Round(GivenRate, 1);

#if WINDOWS_PHONE_APP
                            
                            ProgressHelper.ShowProgressIndicator("Fetching Reviews....");
#endif

                            result = await _qHelper.PostRating(req);

                            isFlyoutOpen = false;
                            //Refresh Ratings feed

#if WINDOWS_PHONE_APP
                            
                            ProgressHelper.ShowProgressIndicator("Refreshing Reviews....");
#endif
                            GetDoctorRatings();
                        }
                        catch (Exception ex)
                        {
                            result = false;
                        }

                        if (!result)
                        {
                            MessageDialog dialog = new MessageDialog("Rating posting failed, kindly try later");
                            await dialog.ShowAsync();
                        }

#if WINDOWS_PHONE_APP
                            
                       ProgressHelper.HideProgressIndicator();
#endif

                    } 
            }
        }

        private string GetHardwareId()
        {
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var hardwareId = token.Id;
            var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);

            byte[] bytes = new byte[hardwareId.Length];
            dataReader.ReadBytes(bytes);

            return BitConverter.ToString(bytes);
        } 
    }
}
