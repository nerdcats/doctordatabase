﻿using DoctorDatabase.Helpers;
using DoctorDatabase.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DoctorDatabase.ViewModel
{
    public class TopDoctorViewModel:ViewModelBase
    {


        public const string CategoryListPropertyName = "CategoryList";

        private ObservableCollection<string> _Categorylist = null;
        public ObservableCollection<string> CategoryList
        {
            get
            {
                return _Categorylist;
            }

            set
            {
                if (_Categorylist == value)
                {
                    return;
                }

          
                _Categorylist = value;
                RaisePropertyChanged(CategoryListPropertyName);
            }
        }

        
        public const string DownloadFailedPropertyName = "DownloadFailed";

        private bool _DownloadFailed = false;
        public bool DownloadFailed
        {
            get
            {
                return _DownloadFailed;
            }

            set
            {
                if (_DownloadFailed == value)
                {
                    return;
                }

               
                _DownloadFailed = value;
                RaisePropertyChanged(DownloadFailedPropertyName);
            }
        }


        public const string GetTopDoctorsCommandPropertyName = "GetTopDoctorsCommand";
        private RelayCommand<string> _GetTopDoctorCommand = null;
        public RelayCommand<string> GetTopDoctorsCommand
        {
            get
            {
                return _GetTopDoctorCommand;
            }

            set
            {
                if (_GetTopDoctorCommand == value)
                {
                    return;
                }

               
                _GetTopDoctorCommand = value;
                RaisePropertyChanged(GetTopDoctorsCommandPropertyName);
            }
        }


        public const string ReloadCommandPropertyName = "ReloadCommand";
        private RelayCommand _reloadCommand = null;
        public RelayCommand ReloadCommand
        {
            get
            {
                return _reloadCommand;
            }

            set
            {
                if (_reloadCommand == value)
                {
                    return;
                }

                
                _reloadCommand = value;
                RaisePropertyChanged(ReloadCommandPropertyName);
            }
        }

       
        public const string IsInProgressPropertyName = "IsInProgress";

        private bool _IsInProgress = false;

       
        public bool IsInProgress
        {
            get
            {
                return _IsInProgress;
            }

            set
            {
                if (_IsInProgress == value)
                {
                    return;
                }

                
                _IsInProgress = value;
                RaisePropertyChanged(IsInProgressPropertyName);
            }
        }

        private QueryHelper _qHelper = new QueryHelper();
        private ViewModelLocator _vmLocator = new ViewModelLocator();
        public TopDoctorViewModel()
        {
            GetTopDoctorsCommand = new RelayCommand<string>(GetTopDoctorsAction);
            ReloadCommand = new RelayCommand(ReloadAction);
        }

        private void ReloadAction()
        {
            LoadExpertise();
        }

        private void GetTopDoctorsAction(string expertise)
        {
            try
            {
               

                dynamic query = new ExpandoObject();

                query.Expertise = expertise;
                query.Sort = QueryHelper.DoctorSortParams.Rating;

                _vmLocator.Search.QueryObject = query;
                Messenger.Default.Send<Type>(typeof(SearchResults), Constants.NavigationToken);



            }
            catch (Exception ex)
            {

                Debug.WriteLine("Unknown Error");

            }
        }

        public async void LoadExpertise()
        {
#if WINDOWS_PHONE_APP
            ProgressIndicatorHelper progressHelper = new ProgressIndicatorHelper();
            progressHelper.ShowProgressIndicator("Refreshing Expertise List...");
#endif
            IsInProgress = true;
            try
            {
                DownloadFailed = false;

                CategoryList = null;

                var expertises = await _qHelper.GetExpertiseList();
                CategoryList = new ObservableCollection<string>(expertises);
                DownloadFailed = false;
            }
            catch (Exception ex)
            {
                DownloadFailed = true;
             
            }

#if WINDOWS_PHONE_APP
            progressHelper.HideProgressIndicator();
#endif
            IsInProgress = false;
        }

        
    }
}
