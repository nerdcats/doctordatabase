﻿using DoctorDatabase.Helpers;
using DoctorDatabase.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using Windows.UI.Popups;

namespace DoctorDatabase.ViewModel
{
    public class FavouritesViewModel:ViewModelBase
    {

        public const string FavouritesEmptyPropertyName = "FavouritesEmpty";
        private bool _favouritesEmpty = false;
        public bool FavouritesEmpty
        {
            get
            {
                return _favouritesEmpty;
            }

            set
            {
                if (_favouritesEmpty == value)
                {
                    return;
                }

                _favouritesEmpty = value;
                RaisePropertyChanged(FavouritesEmptyPropertyName);
            }
        }

        public const string FavouritesCollectionPropertyName = "FavouritesCollection";

        private ObservableCollection<Doctor> _FavouritesCollection = null;
        public ObservableCollection<Doctor> FavouritesCollection
        {
            get
            {
                return _FavouritesCollection;
            }

            set
            {
                if (_FavouritesCollection == value)
                {
                    return;
                }

                
                _FavouritesCollection = value;
                RaisePropertyChanged(FavouritesCollectionPropertyName);
            }
        }

        private ViewModelLocator _vmlocator = new ViewModelLocator();


        public const string ShowDoctorDetailsCommandPropertyName = "ShowDoctorDetailsCommand";

        private RelayCommand<Doctor> _ShowDoctorDetailsCommand = null;
        public RelayCommand<Doctor> ShowDoctorDetailsCommand
        {
            get
            {
                return _ShowDoctorDetailsCommand;
            }

            set
            {
                if (_ShowDoctorDetailsCommand == value)
                {
                    return;
                }
                _ShowDoctorDetailsCommand = value;
                RaisePropertyChanged(ShowDoctorDetailsCommandPropertyName);
            }
        }

        public const string DeleteFromFavouritesCommandPropertyName = "DeleteFromFavouritesCommand";

        private RelayCommand<Doctor> _DeleteFromFavourites = null;
        public RelayCommand<Doctor> DeleteFromFavouritesCommand
        {
            get
            {
                return _DeleteFromFavourites;
            }

            set
            {
                if (_DeleteFromFavourites == value)
                {
                    return;
                }

                
                _DeleteFromFavourites = value;
                RaisePropertyChanged(DeleteFromFavouritesCommandPropertyName);
            }
        }
        public FavouritesViewModel()
        {
           

            ShowDoctorDetailsCommand = new RelayCommand<Doctor>(ShowDoctorsDetailsAction);
            DeleteFromFavouritesCommand = new RelayCommand<Doctor>(DeleteFromFavouritesAction);
        }

        public void LoadFavourites()
        {
            FavouritesCollection = new ObservableCollection<Doctor>(StorageHelper.LoadFavourites());
            if (FavouritesCollection.Count > 0)
                FavouritesEmpty = false;
            else
                FavouritesEmpty = true;
        }



        private void ShowDoctorsDetailsAction(Doctor obj)
        {

            try
            {
                _vmlocator.DoctorDetails.DoctorDetailsDataContext = obj;
                Messenger.Default.Send<Type>(typeof(DoctorDetails), Constants.NavigationToken);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Navigation Failure");

            }

        }

        private void DeleteFromFavouritesAction(Doctor doc)
        {
           if(FavouritesCollection!=null & FavouritesCollection.Count>0)
           {
               if(!FavouritesCollection.Remove(doc))
               {
                   MessageDialog dialog = new MessageDialog("Failed to delete item from Favourites, please try later");
                   dialog.ShowAsync();
               }
               else
               {
                   var list = FavouritesCollection.ToList<Doctor>();
                   StorageHelper.SaveFavourites(list);
                   if (list.Count == 0)
                       FavouritesEmpty = true;
               }
           }
        }


    }
}
