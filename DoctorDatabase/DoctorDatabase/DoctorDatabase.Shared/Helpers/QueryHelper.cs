﻿

namespace DoctorDatabase.Helpers
{

    using DoctorDatabase.Model;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    public class QueryHelper
    {

        private readonly string NerdCatsBase = "http://162.243.223.128:1337/doctor/";
        private readonly string SearchKeyWord = "q";
        private readonly string SkipKeyWord = "sk";
        private readonly string SortKeyword = "so";
        private readonly string RatingKeyword = "r";
        private readonly string AreaParam = "Area";
        private readonly string ExpertiseParam="Expertise";
        private readonly string RatingsExtension = "rating/";

        public enum DoctorSortParams { Expertise, Rating, Name };

        public QueryHelper()
        {
            
        }

        private string GetQuery(object query, int skip=0, DoctorSortParams SortParam = DoctorSortParams.Name)
        {
           
            return string.Concat(NerdCatsBase, "?", SearchKeyWord, "=", WebUtility.UrlEncode(JsonConvert.SerializeObject(query)) , "&", SkipKeyWord, "=", skip.ToString(), "&", SortKeyword, "=", SortParam.ToString()); 

        }

        private string GetQuery(string extension, string keyword, string query)
        {
            return string.Concat(NerdCatsBase,extension, "?",keyword, "=", query); 
        }

        

        public async Task<List<string>> GetAreasList()
        {
            List<string> resultData;

                HttpClient client = new HttpClient();
                resultData = JsonConvert.DeserializeObject<List<string>>(await client.GetStringAsync(GetQuery("",SearchKeyWord, AreaParam)));
            

            return resultData;
        }

        public async Task<List<string>> GetExpertiseList()
        {
            List<string> resultData;


            
                HttpClient client = new HttpClient();
                resultData = JsonConvert.DeserializeObject<List<string>>(await client.GetStringAsync(GetQuery("", SearchKeyWord, ExpertiseParam)));
            

            return resultData;

        }

        public async Task<List<Doctor>> SearchDoctor(object query, int skip=0, DoctorSortParams SortParam = DoctorSortParams.Name)
        {
            List<Doctor> resultData;
          
                HttpClient client = new HttpClient();
                resultData = JsonConvert.DeserializeObject<QueryResponse>(await client.GetStringAsync(GetQuery(query, skip, SortParam))).data;
           
               
                
            

            return resultData;
        }

        public async Task<List<RatingData>> GetRatingFeed(int DoctorId)
        {
            List<RatingData> resultData;
            
              HttpClient client = new HttpClient();
              var Json = await client.GetStringAsync(GetQuery(RatingsExtension, SearchKeyWord, DoctorId.ToString()));
              resultData =JsonConvert.DeserializeObject<List<RatingData>>(Json);
            
           
            return resultData;
        }

        public async Task<bool> PostRating(RatingRequest req)
        {
            var retval=false;

          
                HttpClient client = new HttpClient();
                var query = GetQuery(RatingsExtension, RatingKeyword, WebUtility.UrlEncode(JsonConvert.SerializeObject(req)));
                var feedback = JsonConvert.DeserializeObject<RatingRequestFeedBack>(await client.GetStringAsync(query));
                if (feedback.Status.Equals("OK"))
                    retval = true;
                            
            

            return retval;
        }
    }
}
