﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace DoctorDatabase.Helpers
{

    public class BooleanToVisibilityConverter : IValueConverter
    {

        
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool)value) ? Visibility.Visible : Visibility.Collapsed;
        }
        

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value == Visibility.Visible;
        }
    }

    public class InverseBooleanToVisibilityConverter : IValueConverter
    {


        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool)value) ? Visibility.Collapsed : Visibility.Visible;
        }


        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value != Visibility.Visible;
        }
    }


    public class InverseBooleanConverter : IValueConverter
    {


        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool)value) ? false : true;
        }


        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is bool && (bool)value != true;
        }
    }

    public class RatingToPercentageConverter: IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is double)
                return (double)((double)value / 10.00);
            else
                return value;

        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
