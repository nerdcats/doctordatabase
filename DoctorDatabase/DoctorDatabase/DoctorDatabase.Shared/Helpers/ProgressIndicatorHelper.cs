﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;

namespace DoctorDatabase.Helpers
{
#if WINDOWS_PHONE_APP
    public class ProgressIndicatorHelper
    {

            public void ShowProgressIndicator(string text)
            {
                try
                {
                    StatusBarProgressIndicator progressBar = StatusBar.GetForCurrentView().ProgressIndicator;
                    
                    progressBar.Text = text;
                    progressBar.ShowAsync();
                }
                catch { }
            }

            public void HideProgressIndicator()
            {
                try
                {
                    StatusBarProgressIndicator progressBar = StatusBar.GetForCurrentView().ProgressIndicator;
                    progressBar.HideAsync();

                }
                catch { }
            }
        }


    
#endif
}
