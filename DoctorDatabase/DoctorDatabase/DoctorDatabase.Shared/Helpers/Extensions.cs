﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DoctorDatabase.Helpers
{
    public static class ObservableCollectionExtension
    {
        public static List<T> ToList<T>(this ObservableCollection<T> oc)
        {
            IEnumerable<T> obsCollection = (IEnumerable<T>)oc;
            return new List<T>(obsCollection);
        }
    }

    
}
