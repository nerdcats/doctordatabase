﻿using DoctorDatabase.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;

namespace DoctorDatabase.Helpers
{
    internal static class StorageHelper
    {

       

        private static ApplicationDataContainer Settings = Windows.Storage.ApplicationData.Current.LocalSettings;
       

        public static void Save(string key, object value)
        {
            if(Settings.Values.ContainsKey(key))
            {
                Settings.Values[key] = value;
            }
            else
            {
                Settings.Values.Add(new KeyValuePair<string, object>(key, value));
            }
        }

        public static void SaveFavourites(List<Doctor> FavList)
        {
            if(Settings.Values.ContainsKey(StorageConstants.FavouritesKey))
            {
                Settings.Values[StorageConstants.FavouritesKey] = JsonConvert.SerializeObject(FavList);
            }
            else
            {
                Settings.Values.Add(new KeyValuePair<string, object>(StorageConstants.FavouritesKey, JsonConvert.SerializeObject(FavList)));
            }
        }

        public static object GetData(string key)
        {
            if(Settings.Values.ContainsKey(key))
            {
                return Settings.Values[key];
            }
            else
            {
                return null;
            }
        }

        public static List<Doctor> LoadFavourites()
        {
            if(Settings.Values.ContainsKey(StorageConstants.FavouritesKey))
            {
                var data= JsonConvert.DeserializeObject<List<Doctor>>((string)Settings.Values[StorageConstants.FavouritesKey] );
                return data;
            }
            else
            {
                SaveFavourites( new List<Doctor>());
                var data = JsonConvert.DeserializeObject<List<Doctor>>((string)Settings.Values[StorageConstants.FavouritesKey]);
                return data;
            }
        }
    }
}
